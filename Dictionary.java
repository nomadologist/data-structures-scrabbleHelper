import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

/**
 * Class for reading a dictionary file and searching that dictionary for words and prefixes
 * 
 * @author Jack Booth
 * @version 3/2/2017
 */
public class Dictionary
{
//	Variable declarations
	private Scanner fileInput;
	private ArrayList <String> dic = new ArrayList<String>();
	
	/**
	 * Constructs a new Dictionary object, which contains an array list to populate with words.
	 * @param f - the file to create the dictionary from
	 * @throws IllegalArgumentException - thrown if the dictionary doesn't exist, can't be thrown, or if the file doesn't exist
	 */
	public Dictionary(File f) throws IllegalArgumentException
	{
//		Checks to see if the file is valid
		if(!f.exists() || !f.canRead())
			throw new IllegalArgumentException();

//		Reads the file using Scanner object.  If file isn't found, throws an exception.
		try
		{
			fileInput = new Scanner(f);
			while(fileInput.hasNextLine())
			{
				dic.add(fileInput.nextLine());
			}
		}
		catch(FileNotFoundException e)
		{
			throw new IllegalArgumentException();
		}
	}
	
	/**
	 * Checks the dictionary to see if a string of letters is a word
	 * @param str - the string to check
	 * @return - true if the string is a word
	 */
	public boolean isWord(String str)
	{
		int position = -1;
		position = binaryWordSearch(dic, 0, dic.size(), str);
		
		if(position<0)
			return false;
		else
			return true;
	}
	
	/**
	 * Checks the dictionary to see if a string of letters is a prefix in the dictionary
	 * @param str - the string to check
	 * @return - true if the string is a valid prefix
	 */
	public boolean isPrefix(String str)
	{
		int position = -1;
		position = binaryPrefixSearch(dic, 0, dic.size(), str);
		
		if(position<0)
			return false;
		else
			return true;
		
	}
	
	/**
	 * Recursive function to search a dictionary for a word
	 * @param dic - the dictionary to search
	 * @param beginning - the beginning index from which to search
	 * @param end - the last position from which to search
	 * @param target - the string that is being searched for
	 * @return the position of the word, if there is one.  Otherwise returns -1
	 */
	private int binaryWordSearch(ArrayList<String> dic, int beginning, int end, String target)
	{
		int mid = (beginning + end)/2;
		
//		If word is not found
		if (mid < 0 || mid > dic.size() - 1 || end<beginning)
	        return -1;
		
//		Base Case
		if(target.equals(dic.get(mid)))
		{
			return mid;
		}
		
//		If target comes before point in the dictionary
		else if(target.compareTo(dic.get(mid)) < 0)
		{
			return binaryWordSearch(dic, beginning, mid-1, target);
		}
		
//		If target comes after point in the dictionary
		else 
		{
			return binaryWordSearch(dic, mid+1, end, target);
		}

	}
	
	/**
	 * Recursive function to search a dictionary and see if a string is a prefix
	 * @param dic - the dictionary to search
	 * @param beginning - the beginning index from which to search
	 * @param end - the last position from which to search
	 * @param target - the string that is being searched for
	 * @return the position of the word, if there is one.  Otherwise returns -1
	 */
	private int binaryPrefixSearch(ArrayList<String> dic, int beginning, int end, String target)
	{
		int mid = (beginning + end)/2;
		
//		If word is not found
		if (mid < 0 || mid > dic.size() - 1 || end<beginning)
	        return -1;
		
//		Base Case
		if(dic.get(mid).startsWith(target))
		{
			return mid;
		}
		
//		If target comes before point in the dictionary
		else if(target.compareTo(dic.get(mid)) < 0)
		{
			return binaryPrefixSearch(dic, beginning, mid-1, target);
		}
		
//		If target comes after point in the dictionary
		else 
		{
			return binaryPrefixSearch(dic, mid+1, end, target);
		}
		
	}
}
